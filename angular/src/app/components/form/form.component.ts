import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";

import { Service } from "../../services/service";
import { Model } from "../../models/model";

@Component({
  selector: "app-form",
  templateUrl: "./form.component.html",
  styleUrls: ["./form.component.css"],
  providers: [Service]
})

export class formComponent implements OnInit {
  constructor(private service: Service) { }

  ngOnInit() {
    this.getEmployees();
  }

  addEmployee(form?: NgForm) {
    if (form.value._id) {
      this.service.putEmployee(form.value).subscribe((res) => {
        this.resetForm(form);
        this.getEmployees();
      });
    } else {
      this.service.postEmployee(form.value).subscribe((res) => {
        this.getEmployees();
        this.resetForm(form);
      });
    }
  }

  getEmployees() {
    this.service.getEmployees().subscribe((res) => {
      this.service.employees = res;
    });
  }

  editEmployee(employee: Model) {
    this.service.selectedEmployee = employee;
  }

  deleteEmployee(_id: string, form: NgForm) {
    if (confirm("Are you sure you want to delete it?")) {
      this.service.deleteEmployee(_id).subscribe((res) => {
        this.getEmployees();
        this.resetForm(form);
      });
    }
  }

  resetForm(form?: NgForm) {
    if (form) {
      form.reset();
      this.service.selectedEmployee = new Model();
    }
  }
}