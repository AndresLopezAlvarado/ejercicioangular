import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

// Components
import { appComponent } from './app.component';
import { formComponent } from './components/form/form.component';
import { listComponent } from './components/list/list.component';

@NgModule({
  declarations: [
    appComponent,
    formComponent,
    listComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [appComponent]
})

export class appModule {}