import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

import { Model } from "../models/model";

@Injectable({
    providedIn: "root",
})

export class Service {
    selectedEmployee: Model;
    // employees: Employee[];
    employees: Model[] = [];
    readonly URL_API = "http://localhost:3000/api/employees";

    constructor(private http: HttpClient) {
        this.selectedEmployee = new Model();
    }

    postEmployee(employee: Model) {
        return this.http.post(this.URL_API, employee);
    }

    getEmployees() {
        return this.http.get<Model[]>(this.URL_API);
    }

    putEmployee(employee: Model) {
        return this.http.put(this.URL_API + `/${employee._id}`, employee);
    }

    deleteEmployee(_id: string) {
        return this.http.delete(this.URL_API + `/${_id}`);
    }
}