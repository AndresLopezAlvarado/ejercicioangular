import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { listComponent } from './components/list/list.component';
import { formComponent } from './components/form/form.component';

// const routes: Routes = [];

@NgModule({
  declarations: [
    listComponent,
    formComponent
  ],
  imports: [RouterModule.forRoot([
    { path: 'list', component: listComponent },
    { path: 'form', component: formComponent },
  ])],
  exports: [RouterModule]
})

export class appRoutingModule { } 