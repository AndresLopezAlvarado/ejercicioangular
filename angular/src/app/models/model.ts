export class Model {
  constructor(_id = "", name = "", email = "", phone = 0, subject = "", message = "") {
    this._id = _id;
    this.name = name;
    this.email = email;
    this.phone = phone;
    this.subject = subject;
    this.message = message;
  }

  _id: string;
  name: string;
  email: string;
  phone: number;
  subject: string;
  message: string;
}