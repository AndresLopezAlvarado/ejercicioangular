###

GET http://localhost:3100/api/employees HTTP/1.1

###

POST http://localhost:3100/api/employees HTTP/1.1
Content-Type: application/json

{
    "name": "Andres",
    "email": "Andres@a.com",
    "phone": "54654654",
    "subject": "ASunto",
    "message": "Mensaje"
}

### 

GET http://localhost:3100/api/employees/5b0624a7abcb202a3af189b6 HTTP/1.1

###

PUT http://localhost:3100/api/employees/64c867817753b0a947d23734
Content-Type: application/json

{
    "name": "Carlos",
    "email": "Carlos@a.com",
    "phone": "54654564",
    "subject": "ASunto2",
    "message": "Mensaje2"
}

###
DELETE http://localhost:3100/api/employees/64c867817753b0a947d23734