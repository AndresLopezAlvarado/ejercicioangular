const express = require("express");
const cors = require('cors');
const morgan = require('morgan');

require("./database");

const app = express();

app.set('port', process.env.PORT || 3100);

app.use(cors());
app.use(morgan("dev"));
app.use(express.json());

app.use(require('./routes/route'));
app.use("/api/employees", require("./routes/route"));

app.listen(app.get("port"), () => {
    console.log(`Server on port backend ${app.get("port")}`);
});
// module.exports = app;