const mongoose = require("mongoose");

const URI = "mongodb://127.0.0.1/dbAngular";

mongoose
    .connect(URI, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
    .then((database) => console.log("Database is connected Backend"))
    .catch((error) => console.log(error));

module.exports = mongoose;