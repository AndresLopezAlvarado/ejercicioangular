const express = require("express");
const router = express.Router();

const Employee = require("../models/model");

const employee = require("../controllers/controller");

router.get("/", employee.getEmployees);

router.post("/", employee.createEmployee);



router.get('/', async (request, response) => {
    const arrayMongoDBEmployees = await Employee.find();
    response.render('../', { arrayMongoDBEmployees });
});



router.get("/:id", employee.getEmployee);

router.put("/:id", employee.editEmployee);

router.delete("/:id", employee.deleteEmployee);

module.exports = router;